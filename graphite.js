var net = require('net');

var GraphiteClient = module.exports = function(host, port) {

	this.host = host;
	this.port = port;
	this.connection = null;
	this.data = {};
};

GraphiteClient.prototype.connect = function(callback) {

	var self = this;

	if (null === this.connection) {
		var conn = new net.Socket();
		conn.connect(this.port, this.host, function() {

			conn.on('close', function() {
				console.log('connection to ' + self.host + ' closed.');
				self.connection = null;
			});

			self.connection = conn;	
			callback();
		});
	}
};

GraphiteClient.prototype.close = function() {

	if (this.connection !== null) {
		this.connection.destroy();
		this.connection = null;
	}
};

GraphiteClient.prototype.push = function(input, callback) {

	var self = this;
	var dict = input;

	var write = function() {

		var data = null;
		var strBuilder = [];

		for (var key in dict) {
			if (dict.hasOwnProperty(key)) {
				strBuilder.push(self.format(key, dict[key]));
			}
		}

		data = strBuilder.join('');
		console.log(data);
		self.connection.write(data, 'utf-8', callback);	
	}	

	// if we're not connected chain it after the connect
	if (null === this.connection) {
		this.connect(write);
	}
	else {
		write();
	}	
};

// todo: dict with functions 
GraphiteClient.prototype.update = function() {

	var dict = [];

	for (key in this.data) {
		if (this.data.hasOwnProperty(key)) {			
			dict[key] = this.data[key]();
		}
	}

	this.push(dict);
};

GraphiteClient.prototype.register = function(varName, getter) {

	if (typeof getter !== 'function') {
		throw 'getter is no function';
	}

	this.data[varName] = getter;
};

GraphiteClient.prototype.registerBlock = function(dict) {

	for (key in dict) {
		this.register(key, dict[key]);
	}
};

GraphiteClient.prototype.unregister = function(varName) {

	delete this.data[varName];
};

GraphiteClient.prototype.unregisterBlock = function(dict) {
	for (key in dict) {
		this.unregister(key);
	}
};

GraphiteClient.prototype.format = function(varName, varValue, timestamp) {

	return [varName, varValue, timestamp || this.getTimestamp()].join(' ') + '\n';
};

GraphiteClient.prototype.getTimestamp = function() {

	return Date.now()/1000 | 0;	// prepare timestamp for carbon
};

